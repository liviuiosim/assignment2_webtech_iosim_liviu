function addTokens(input, tokens) {
  if (typeof input !== "string") {
    throw new Error("Invalid input");
  }
  if (input.length < 6) {
    throw new Error("Input should have at least 6 characters");
  }

  // const areAllStrings = tokens.every(x => typeof x === "string");
  // if (areAllStrings === false && Array.isArray(tokens)) {
  //   throw Error("Invalid array format");
  // }

  if (input.includes("...") === false) {
    return input;
  } else {
    let i = 0;
    var out = input.replace(
      /\.\.\./g,
      () => "${" + Object.values(tokens[i++]) + "}"
    ); //uses regex expression for finding the dots
    return out;
  }
}

var a = "Subsemnatul ... lll ...";
var b = [{ tokenName: "subsemnatul" }, { aa: "susasabsemnatul" }];
console.log(addTokens(a, b));

const app = {
  addTokens: addTokens
};

module.exports = app;
